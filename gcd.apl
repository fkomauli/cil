pred gcd (fun\prog).
gcd(main\([
    (int, x),
    (int, y)
], [
    (int, subtr, [(int, a), (int, b)],
        [(int, r)], [
        (l1, set_var(r, bop(op_sub, var(a), var(b)))),
        (l2, return(var(r)))
    ]),
    (void, main, [],
        [], [
        (l3, if(bop(op_neq, var(x), var(y)), l4, l9)),
        (l4, if(bop(op_gt, var(x), var(y)), l5, l7)),
        (l5, call(x, subtr, [var(x), var(y)])),
        (l6, goto(l3)),
        (l7, call(x, subtr, [var(y), var(x)])),
        (l8, goto(l3)),
        (l9, halt)
    ])
])).


? gcd(main\P), initial(main\P) = C.

? gcd(main\P), initial(main\P) = C, step(P, C) = C'.

? gcd(main\P), run_prog(main\P) = C.
