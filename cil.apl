%% SYNTAX %%

var: name_type.
avar: name_type.
fun: name_type.
label: name_type.

ty: type.
void: ty.
int: ty.

uop: type.

bop: type.
op_add: bop.
op_sub: bop.
op_eq: bop.
op_neq: bop.
op_lt: bop.
op_gt: bop.
op_leq: bop.
op_geq: bop.

type decl = (ty, var).

expr: type.
const: int_val -> expr.
var: var -> expr.
uop: (uop, expr) -> expr.
bop: (bop, expr, expr) -> expr.
fetch: (avar, expr) -> expr.

cmd: type.
set_var: (var, expr) -> cmd.
set_avar: (avar, expr, expr) -> cmd.
call: (var, fun, [expr]) -> cmd.
goto: label -> cmd.
if: (expr, label, label) -> cmd.
return: expr -> cmd.
abort: cmd.
halt: cmd.

type lab_cmd = (label, cmd).

type fundef = (ty, fun, [decl], [decl], [lab_cmd]).

type prog = ([decl], [fundef]). % type system will enforce latter not empty and with main


%% OPERATIONAL SEMANTICS %%

binding: type.
bind_int: (var, int_val) -> binding.
bind_arr: (avar, nat_val, [int_val]) -> binding.

type global_env = [binding].

type local_env = [binding].

type env = (global_env, local_env).

configuration: type.
regular_execution: (lab_cmd, env) -> configuration.
aborted_execution: (lab_cmd, env) -> configuration.

func upd ([binding], binding) = [binding].
upd([bind_int(V, _) | E], bind_int(V, A)) = [bind_int(V, A) | E].
upd([bind_int(V, A) | E], bind_int(V', A')) = [bind_int(V, A) | upd(E, bind_int(V', A'))] :- V # V'.
upd([bind_arr(V, N, A) | E], bind_int(V', A')) = [bind_arr(V, N, A) | upd(E, bind_int(V', A'))].
upd([bind_arr(V, _, _) | E], bind_arr(V, N, A)) = [bind_arr(V, N, A) | E].
upd([bind_arr(V, N, A) | E], bind_arr(V', N', A')) = [bind_arr(V, N, A) | upd(E, bind_arr(V', N', A'))] :- V # V'.
upd([bind_int(V, A) | E], bind_arr(V', N', A')) = [bind_int(V, A) | upd(E, bind_arr(V', N', A'))].

func update (env, binding) = env.
update((G, L), B) = (upd(G, B), L).
update((G, L), B) = (G, upd(L, B)).

func write ([int_val], nat_val, int_val) = [int_val].
write([_ | A'], zero, A) = [A | A'].
write([A | A'], succ(N), A'') = [A | write(A', N, A'')].

func at (prog, label) = lab_cmd.
at((_, [(_, _, _, _, [(L, C) | C']) | F]), L) = (L, C).
at((_, [(_, _, _, _, [(L, C) | C']) | F]), L') = at((_, [(_, _, _, _, C') | F]), L') :- L # L'.
at((_, [(_, _, _, _, []) | F]), L) = at((_, F), L).

func nextlab (prog, label) = lab_cmd.
nextlab((_, [(_, _, _, _, [(L, _), (L', C) | C']) | F]), L) = (L', C).
nextlab((_, [(_, _, _, _, [(L, _) | C']) | F]), L') = nextlab((_, [(_, _, _, _, C') | F]), L') :- L # L'.

func firstlab (prog, fun) = lab_cmd.
firstlab((_, [(_, F, _, _, [(L, C) | _]) | _]), F) = (L, C).
firstlab((_, [(_, F, _, _, _) | P]), F') = firstlab((_, P), F') :- F # F'.

func get_int (env, var) = int_val.
get_int(([bind_int(V, A) | _], _), V) = A.
get_int(([bind_int(V, A) | G], L), V') = get_int((G, L), V') :- V # V'.
get_int(([bind_arr(_, _, _) | G], L), V) = get_int((G, L), V).
get_int(([], [bind_int(V, A) | _]), V) = A.
get_int(([], [bind_int(V, A) | L]), V') = get_int(([], L), V') :- V # V'.
get_int(([], [bind_arr(_, _, _) | L]), V) = get_int(([], L), V).

func get_arr (env, avar) = (nat_val, [int_val]).
get_arr(([bind_arr(V, N, A) | _], _), V) = (N, A).
get_arr(([bind_arr(V, N, A) | G], L), V') = get_arr((G, L), V') :- V # V'.
get_arr(([bind_int(_, _) | G], L), V) = get_arr((G, L), V).
get_arr(([], [bind_arr(V, N, A) | _]), V) = (N, A).
get_arr(([], [bind_arr(V, N, A) | L]), V') = get_arr(([], L), V') :- V # V'.
get_arr(([], [bind_int(_, _) | L]), V) = get_arr(([], L), V).

func extract ([int_val], nat_val) = int_val.
extract([A | _], zero) = A.
extract([_ | A], succ(I)) = extract(A, I).

func eval (expr, env) = int_val.
eval(const(N), _) = N.
eval(var(V), E) = get_int(E, V).
% no uop defined yet
eval(bop(op_add, X, Y), E) = add(A, B) :- A = eval(X, E), B = eval(Y, E).
eval(bop(op_sub, X, Y), E) = sub(A, B) :- A = eval(X, E), B = eval(Y, E).
eval(bop(op_eq, X, Y), E) = eq(A, B) :- A = eval(X, E), B = eval(Y, E).
eval(bop(op_neq, X, Y), E) = neq(A, B) :- A = eval(X, E), B = eval(Y, E).
eval(bop(op_lt, X, Y), E) = lt(A, B) :- A = eval(X, E), B = eval(Y, E).
eval(bop(op_gt, X, Y), E) = gt(A, B) :- A = eval(X, E), B = eval(Y, E).
eval(bop(op_leq, X, Y), E) = leq(A, B) :- A = eval(X, E), B = eval(Y, E).
eval(bop(op_geq, X, Y), E) = geq(A, B) :- A = eval(X, E), B = eval(Y, E).
eval(fetch(V, I), E) = X :- (N, A) = get_arr(E, V), J = eval(I, E), X = extract(A, int_to_nat(J)).


%% MULTISTEP SEMANTICS %%

func fun_args (prog, fun) = [decl].
fun_args((_, [(_, F, D, _, _) | _]), F) = D.
fun_args((GD, [(_, F, _, _, _) | P]), F') = fun_args((GD, P), F') :- F # F'.

func fun_locals (prog, fun) = [decl].
fun_locals((_, [(_, F, _, L, _) | _]), F) = L.
fun_locals((GD, [(_, F, _, _, _) | P]), F') = fun_locals((GD, P), F') :- F # F'.

func init_locals ([decl]) = local_env.
init_locals([]) = [].
init_locals([(_, V) | VS]) = [bind_int(V, z) | init_locals(VS)].

func eval_args(env, [decl], [expr], local_env) = local_env.
eval_args(_, [], [], L) = L.
eval_args(E, [(_, V) | VS], [X | XS], L) = [bind_int(V, eval(X, E)) | eval_args(E, VS, XS, L)].

func fun_env (prog, fun, env, [expr]) = local_env.
fun_env(P, F, E, []) = init_locals(fun_locals(P, F)).
fun_env(P, F, E, [X | XS]) = eval_args(E, fun_args(P, F), [X | XS], init_locals(fun_locals(P, F))).

func choose_label (int_val, label, label) = label.
choose_label(z, _, L) = L.
choose_label(pos(_), L, _) = L.
choose_label(neg(_), L, _) = L.

func fun_result(prog, env, configuration) = configuration.
fun_result(P, (GE, LE), aborted_execution((L', abort), (GE', LE'))) = aborted_execution((L', abort), (GE', LE)).
fun_result(P, (GE, LE), regular_execution((_, return(E)), (GE', LE'))) = regular_execution(nextlab(P, L), update((GE', LE), bind_int(V, eval(E, (GE', LE'))))).

func step (prog, configuration) = configuration.
func steps (prog, configuration) = configuration.

step(P, regular_execution((L, set_var(V, X)), E)) = regular_execution(nextlab(P, L), update(E, bind_int(V, eval(X, E)))).
step(P, regular_execution((L, set_avar(V, I, X)), E)) = regular_execution(nextlab(P, L), update(E, bind_arr(V, N, A'))) :-
    (N, A) = get_arr(E, V), A' = write(A, N, eval(X, E)).
step(P, regular_execution((L, call(V, F, X)), (GE, LE))) = fun_result(P, (GE, LE), steps(P, regular_execution(firstlab(P, F), (GE, fun_env(P, F, (GE, LE), X))))).
step(P, regular_execution((L, abort), E)) = aborted_execution((L, abort), E).
step(P, regular_execution((L, if(X, L', L'')), E)) = regular_execution(at(P, choose_label(eval(X, E), L', L'')), E).
step(P, regular_execution((L, goto(L')), E)) = regular_execution(at(P, L'), E).

steps(P, C) = C.
steps(P, C) = C'' :- step(P, C) = C', steps(P, C') = C''.


pred step_or_halt (prog, configuration).
step_or_halt(_, regular_execution((_, halt), _)).
step_or_halt(_, regular_execution((_, abort), _)).
step_or_halt(P, regular_execution(LC, E)) :- step(P, regular_execution(LC, E)) = regular_execution(_, _).


func run (prog, configuration) = configuration.
run(_, regular_execution((L, halt), E)) = regular_execution((L, halt), E).
run(P, C) = run(P, step(P, C)).


func initial (fun\prog) = configuration.
initial(main\([], FS)) = regular_execution(firstlab(([], FS), main), ([], init_locals(fun_locals(([], FS), main)))).
initial(main\([(_, V) | DS], FS)) = regular_execution(LC, ([bind_int(V, z) | GE], LE)) :- initial(main\(DS, FS)) = regular_execution(LC, (GE, LE)).


func run_prog (fun\prog) = configuration.
run_prog(main\P) = run(P, initial(main\P)).
