pred sample_prog (prog).
sample_prog([
    (int, x),
    (int, y)
], [
    (int, f, [(int, n)],
        [(int, r)], [
        (l0, if(bop(op_leq, var(n), const(z)), l1, l3)),
        (l1, set_var(r, const(z))),
        (l2, goto(l5)),
        (l3, call(r, f, [bop(op_sub, var(n), const(neg(o)))])),
        (l4, set_var(r, bop(op_add, var(r), var(n)))),
        (l5, return(var(r)))
    ]),
    (void, sum_upto, [],
        [], [
        (l6, call(y, f, [var(x)]))
    ])
]).
