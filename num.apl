nat_val: type.
zero: nat_val.
succ: nat_val -> nat_val.

non_zero: type.
o: non_zero.
s: non_zero -> non_zero.

int_val: type.
z: int_val.
pos: non_zero -> int_val.
neg: non_zero -> int_val.

func int_to_nat (int_val) = nat_val.
int_to_nat(z) = zero.
int_to_nat(pos(o)) = succ(zero).
int_to_nat(pos(s(N))) = succ(int_to_nat(pos(N))).

func int_not (int_val) = int_val.
int_not(z) = pos(o).
int_not(pos(_)) = z.
int_not(neg(_)) = z.

func add_nat (nat_val, nat_val) = nat_val.
add_nat(zero, B) = B.
add_nat(succ(A), zero) = succ(A).
add_nat(succ(A), succ(B)) = succ(succ(C)) :- C = add_nat(A, B).

func add (int_val, int_val) = int_val.
func sub (int_val, int_val) = int_val.
add(A, z) = A.
add(z, pos(B)) = pos(B).
add(pos(o), pos(B)) = pos(s(B)).
add(pos(s(A)), pos(B)) = add(pos(A), pos(s(B))).
add(pos(A), neg(B)) = sub(pos(A), pos(B)).
add(neg(A), pos(B)) = sub(pos(B), pos(A)).
add(neg(A), neg(B)) = neg(C) :- pos(C) = add(pos(A), pos(B)).
sub(A, z) = A.
sub(A, neg(B)) = add(A, pos(B)).
sub(z, pos(B)) = neg(B).
sub(pos(o), pos(s(B))) = neg(B).
sub(pos(s(A)), pos(s(B))) = sub(pos(A), pos(B)).
sub(neg(A), pos(s(B))) = sub(neg(s(A)), pos(B)).

func eq (int_val, int_val) = int_val.
eq(z, z) = pos(o).
eq(z, pos(_)) = z.
eq(z, neg(_)) = z.
eq(pos(_), z) = z.
eq(neg(_), z) = z.
eq(pos(o), pos(o)) = pos(o).
eq(pos(s(A)), pos(s(B))) = eq(pos(A), pos(B)).
eq(neg(A), neg(B)) = eq(pos(A), pos(B)).
eq(pos(_), neg(_)) = z.
eq(neg(_), pos(_)) = z.

func neq (int_val, int_val) = int_val.
neq(A, B) = int_not(eq(A, B)).

func lt (int_val, int_val) = int_val.
lt(pos(_), neg(_)) = z.
lt(neg(_), pos(_)) = pos(o).
lt(pos(_), z) = z.
lt(neg(_), z) = pos(o).
lt(z, z) = z.
lt(z, pos(_)) = pos(o).
lt(z, neg(_)) = z.
lt(pos(o), pos(o)) = z.
lt(pos(s(_)), pos(o)) = z.
lt(pos(o), pos(s(_))) = pos(o).
lt(pos(s(A)), pos(s(B))) = lt(pos(A), pos(B)).
lt(neg(A), neg(B)) = lt(pos(B), pos(A)).

func gt (int_val, int_val) = int_val.
gt(A, B) = lt(B, A).

func leq (int_val, int_val) = int_val.
leq(neg(_), pos(_)) = pos(o).
leq(pos(_), neg(_)) = z.
leq(pos(_), z) = z.
leq(neg(_), z) = pos(o).
leq(z, z) = pos(o).
leq(z, pos(_)) = pos(o).
leq(z, neg(_)) = z.
leq(pos(o), pos(o)) = pos(o).
leq(pos(s(_)), pos(o)) = z.
leq(pos(o), pos(s(_))) = pos(o).
leq(pos(s(A)), pos(s(B))) = leq(pos(A), pos(B)).
leq(neg(A), neg(B)) = leq(pos(B), pos(A)).

func geq (int_val, int_val) = int_val.
geq(A, B) = leq(B, A).
