%%%%%%%% NUMBERS %%%%%%%%

#check "add identity right" 10: add(A, z) = A.

#check "add identity left" 10: add(z, A) = A.

#check "add commutative" 10: add(A, B) = add(B, A).

#check "add associative" 10: add(add(A, B), C) = add(A, add(B, C)).

#check "sub inverse add" 10: add(A, B) = C => sub(C, B) = A.

#check "eq reflexivity" 10: eq(A, A) = pos(o).

#check "eq transitivity" 10: eq(A, B) = pos(o), eq(B, C) = pos(o) => eq(A, C) = pos(o).

#check "lt transitivity" 10: lt(A, B) = pos(o), lt(B, C) = pos(o) => lt(A, C) = pos(o).

#check "gt transitivity" 10: gt(A, B) = pos(o), gt(B, C) = pos(o) => gt(A, C) = pos(o).

#check "leq reflexivity" 10: leq(A, A) = pos(o).

#check "leq transitivity" 10: leq(A, B) = pos(o), leq(B, C) = pos(o) => leq(A, C) = pos(o).

#check "geq reflexivity" 10: geq(A, A) = pos(o).

#check "geq transitivity" 10: geq(A, B) = pos(o), geq(B, C) = pos(o) => geq(A, C) = pos(o).


%%%%%%%% STEP %%%%%%%%

#check "step deterministic" 6: step(P, C) = C', step(P, C) = C'' => C' = C''.

#check "set_var updates environment" 6: N = eval(X, E), step(P, regular_execution((_, set_var(V, X)), E)) = regular_execution(_, E') => get_int(E, V) = N.
